var mysql = require('mysql');
var dbConfig = config.get('DatabaseSettings');
var sendResponse = require('./sendResponse');
var pool = mysql.createPool(dbConfig);

exports.Query = function (res, query, values, callback) {

    pool.getConnection(function (err, connection) {

        if (err) {
            console.log(err);
            sendResponse.somethingWentWrongError(res);
        }
        else {

            connection.query(query, values, function (err, rows) {

                // And done with the connection.
                connection.release();

                if (err) {
                    console.log(err);
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    return callback(rows);
                }
                // Don't use the connection here, it has been returned to the pool.
            });
        }
        // connected! (unless `err` is set)
    });
};